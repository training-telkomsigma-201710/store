package id.artivisi.training.telkomsigma.marketplace.store.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ShoppingCart {
    private List<CartItem> isiCart = new ArrayList<>();

    public BigDecimal totalBerat(){
        BigDecimal total = BigDecimal.ZERO;
        for (CartItem ci : isiCart) {
            total = total.add(ci.getProduct().getWeight().multiply(BigDecimal.valueOf(ci.getJumlah())));
        }
        return total;
    }
}
