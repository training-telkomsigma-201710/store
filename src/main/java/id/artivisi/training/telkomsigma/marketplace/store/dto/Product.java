package id.artivisi.training.telkomsigma.marketplace.store.dto;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private String id;
    private String code;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
}
